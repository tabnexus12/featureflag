package main

import (
    "fmt"
    "github.com/Unleash/unleash-client-go/v3"
)

type metricsInterface struct {
}

    
func main() {
     unleash.Initialize(
        unleash.WithUrl("https://gitlab.com/api/v4/feature_flags/unleash/33241909"),
        unleash.WithInstanceId("9Ra4eTDjTU_pJDsymDfh"),
        unleash.WithAppName("DEV"), // Set to the running environment of your application
        unleash.WithListener(&metricsInterface{}),
    )
     if unleash.IsEnabled("f-mvp-1-1") {
        fmt.Println("Welcome to the MVP 1.1 HomePage!")
		
    } else {
       fmt.Println("MVP 1.1 Feature is disabled!")
    }
}
